from django.conf.urls import patterns, url
from weekopgave_1_opgave_5_cookieless.sessieview import sessie_form_get_cookieless, sessie_form_post_cookieless
from cookieless.decorators import no_cookies

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^sessie_form_post_cookieless$', no_cookies(sessie_form_post_cookieless)),
    url(r'^sessie_form_get_cookieless$', no_cookies(sessie_form_get_cookieless)),
    # Examples:
    # url(r'^$', 'weekopgave_1_opgave_5_cookieless.views.home', name='home'),
    # url(r'^weekopgave_1_opgave_5_cookieless/', include('weekopgave_1_opgave_5_cookieless.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
