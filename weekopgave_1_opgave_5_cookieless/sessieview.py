from django.shortcuts import render

def sessie_form_get_cookieless(request):
    error = ''
    sessie_key = ''
    sessie_value = ''
    if 'add' in request.GET:
        if 'sessie_key' in request.GET and 'sessie_value' in request.GET:
            sessie_key = request.GET['sessie_key']
            sessie_value = request.GET['sessie_value']
            if sessie_key != '' and sessie_value != '':
                request.session[sessie_key] = sessie_value
            else:
                error = 'Je moet een sesie key en een sessie value invullen'             
    elif 'clear' in request.GET:
        request.session.flush()
        
    sessie_keys = request.session.items()
    return render(request, 'sessie_form_get_cookieless.html', {'error': error, 'sessie_keys': sessie_keys, 'sessie_key': sessie_key, 'sessie_value': sessie_value})

def sessie_form_post_cookieless(request):
    error = ''
    sessie_key = ''
    sessie_value = ''
    if 'add' in request.POST:
        if 'sessie_key' in request.POST and 'sessie_value' in request.POST:
            sessie_key = request.POST['sessie_key']
            sessie_value = request.POST['sessie_value']
            if sessie_key != '' and sessie_value != '':
                request.session[sessie_key] = sessie_value
            else:
                error = 'Je moet een sesie key en een sessie value invullen'             
    elif 'clear' in request.POST:
        request.session.flush()
        
    sessie_keys = request.session.items()
    return render(request, 'sessie_form_post_cookieless.html', {'error': error, 'sessie_keys': sessie_keys, 'sessie_key': sessie_key, 'sessie_value': sessie_value})